package renatsayf.example_dynamic_fragments;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity
{
    Fragment_1 fragment1;
    Fragment_2 fragment2;
    FragmentTransaction transaction;
    Button button1, button2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews()
    {
        button1 = (Button) findViewById(R.id.btn_frag_first);
        button2 = (Button) findViewById(R.id.btn_frag_second);
        Button_OnClick();

    }

    private void Button_OnClick()
    {
        button1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (fragment1 == null)
                {
                    fragment1 = new Fragment_1();
                }
                transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, fragment1);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        button2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (fragment2 == null)
                {
                    fragment2 = new Fragment_2();
                }
                transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, fragment2);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }
}
